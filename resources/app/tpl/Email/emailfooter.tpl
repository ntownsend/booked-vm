{*
Copyright 2011-2015 Nick Korbel

This file is part of Booked Scheduler.

Booked Scheduler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Booked Scheduler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
*}
<span style="color:black;border-top:1px solid black;border-radius:0px;text-align:center;position:absolute;clear:both;background-color:#fff;font-family:Arial;font-size:0.9em;width:90%;padding:7px;margin:0;margin-top:80px;">
<b>For all studio related enquiries you can contact us:</b><br ><br>
  &#127970;  Studio Office (COM121) &nbsp;  &#9742; <a style='color:darkblue;text-decoration:none;' href="tel:&#43;441223698599">&#43;441223 698 599 </a> &nbsp; &#128231; <a style='color:darkblue;text-decoration:none;' href="mailto:studio@anglia.ac.uk">studios@anglia.ac.uk</a><br />
  You can find more information on our facilites and equipment by visiting the <a href='https://vle.anglia.ac.uk/courses/amt/studios/'>Studio VLE</a>
</span>

	</body>
</html>

