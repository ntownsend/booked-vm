{*
Copyright 2011-2017 Nick Korbel

This file is part of Booked Scheduler.

Booked Scheduler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Booked Scheduler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
*}
<br />Hi <b>{$FirstName}</b>
<h4>Date / time:</h4>
  <ul>
   <li style=''>{formatdate date=$StartDate key=reservation_email_aru} &rarr;  {formatdate date=$EndDate key=reservation_email_aru}</li>
  </ul>

<h4>
  {if $ResourceNames|count > 1}
  Resources:
  {else}
  Resource:
  {/if}
</h4>

<ul>
  {if $ResourceNames|count > 1}
    {foreach from=$ResourceNames item=resourceName}
      <li>{$resourceName}</li>
    {/foreach}
  {else}
    <li>{$ResourceName}</li>
  {/if}
</ul>

{if $Title}
  Title: {$Title}<br/>
{/if}

{if $Description}
  Description: {$Description|nl2br}<br/>
{/if}

{if count($RepeatDates) gt 0}
  <br/>
  The following dates have been removed:
  <br/>
{/if}

{foreach from=$RepeatDates item=date name=dates}
  {formatdate date=$date}
  <br/>
{/foreach}

{if $Accessories|count > 0}
  <h4>Accessories:</h4>
  {foreach from=$Accessories item=accessory}
    ({$accessory->QuantityReserved}) {$accessory->Name}
    <br/>
  {/foreach}
{/if}
<br />
Your booking (above) has now been cancelled.
<br />
{if !empty($CreatedBy)}
  <br/>
  You were served today by: <b>{$CreatedByFn}</b>
{/if}
<br />
