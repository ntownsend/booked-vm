{*
Copyright 2013-2017 Nick Korbel

This file is part of Booked Scheduler.

Booked Scheduler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Booked Scheduler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
*}
<span style='font-family:Arial'>
<br />Hi <b>{$FirstName}</b><br />
<br />
This is a friendly reminder that your booking with us will be ending soon:

You booked <b>{$ResourceName}</b><br/> {formatdate date=$StartDate key=reservation_email_aru} &rarr;  {formatdate date=$EndDate key=reservation_email_aru}</li>

<h4>Accessories:</h4>
  {foreach from=$Accessories item=accessory}
    ({$accessory->QuantityReserved}) {$accessory->Name}
    <br/>
  {/foreach}

<br/>
<h4>Other info:</h4>
If you need to change your booking, or cancel it, please contact us as soon as possible, quoting your booking reference number: <b style='border:1px dotted #ddd;padding:3px;font$
