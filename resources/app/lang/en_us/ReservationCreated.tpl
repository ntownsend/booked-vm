{*
Copyright 2011-2017 Nick Korbel

This file is part of Booked Scheduler.

Booked Scheduler is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Booked Scheduler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Booked Scheduler.  If not, see <http://www.gnu.org/licenses/>.
*}
<span style='font-family:Arial'>
<br />Hi <b>{$FirstName}</b>
<p>This email confirms your booking with us.</p>
      <h4>Date / time:</h4>
	<ul>
	 <li style=''>{formatdate date=$StartDate key=reservation_email_aru} &rarr;  {formatdate date=$EndDate key=reservation_email_aru}</li>
	</ul>
	<h4>
	{if $ResourceNames|count > 1}
	Resources:
	{else}
	Resource:
	{/if}
	</h4>
        <ul>
	{if $ResourceNames|count > 1}
		{foreach from=$ResourceNames item=resourceName}
			<li>{$resourceName}</li>
		{/foreach}

		{else}
		<li>{$ResourceName}</li>
	{/if}
        </ul>
	{if $Title}
        Title: {$Title}<br/>
        {/if}
        {if $Description}
	Description: {$Description|nl2br}<br/>
        {/if}
	{if count($RepeatDates) gt 0}
		<br/>
		The reservation occurs on the following dates:
		<br/>
	{/if}

	{foreach from=$RepeatDates item=date name=dates}
		{formatdate date=$date}<br/>
	{/foreach}

	{if $Accessories|count > 0}
		<h4>Accessories:</h4>
		{foreach from=$Accessories item=accessory}
			({$accessory->QuantityReserved}) {$accessory->Name}<br/>
		{/foreach}
	{/if}

	{if $Attributes|count > 0}
	<br/>
    	        {foreach from=$Attributes item=attribute}
                {if $attribute}
			<div>{control type="AttributeControl" attribute=$attribute readonly=true}</div>
                {/if}
		{/foreach}
	{/if}

	{if $RequiresApproval}
		<br/>
		One or more of the resources reserved require approval before usage.  This reservation will be pending until it is approved.
	{/if}
	{if $CheckInEnabled}
		<br/>
		At least one of the resources reserved requires you to check in and out of your reservation.
		{if $AutoReleaseMinutes != null}
			This reservation will be cancelled unless you check in within {$AutoReleaseMinutes} minutes after the scheduled start time.
		{/if}
	{/if}

	{if !empty($ApprovedBy)}
		<br/>
		Approved by: {$ApprovedBy}
	{/if}
	<h4>Other info:</h4>
	If you need to change your booking, or cancel it, please contact us as soon as possible, quoting your booking reference number: <b style='border:1px dotted #ddd;padding:3px;font-family:courier, monospaced'>{$ReferenceNumber}</b>


	{if !empty($CreatedBy)}
		<br/><br />
		You were served today by: <b>{$CreatedByFn}</b>
	{/if}

	<br/>
