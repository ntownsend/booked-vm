<?php
$conf['settings']['domain.controllers'] = 'ldap-cam.anglia.local';
$conf['settings']['port'] = '389';
$conf['settings']['username'] = 'studios';
$conf['settings']['password'] = 'p0t4t0.54l4d';
$conf['settings']['basedn'] = 'DC=ANGLIA,DC=LOCAL';
$conf['settings']['version'] = '3';
$conf['settings']['use.ssl'] = 'false';
$conf['settings']['account.suffix'] = '@anglia.local';
$conf['settings']['database.auth.when.ldap.user.not.found'] = 'true';
$conf['settings']['attribute.mapping'] = 'sn=sn,givenname=givenname,mail=mail,telephonenumber=telephonenumber,physicaldeliveryofficename=physicaldeliveryofficename,title=title';
$conf['settings']['required.groups'] = '';
$conf['settings']['sync.groups'] = 'true';
?>
