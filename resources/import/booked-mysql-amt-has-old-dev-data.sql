-- MySQL dump 10.13  Distrib 5.5.52, for Linux (x86_64)
--
-- Host: localhost    Database: bookedscheduler
-- ------------------------------------------------------
-- Server version	5.5.52

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accessories`
--

DROP TABLE IF EXISTS `accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accessories` (
  `accessory_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `accessory_name` varchar(85) NOT NULL,
  `accessory_quantity` smallint(5) unsigned DEFAULT NULL,
  `legacyid` char(16) DEFAULT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accessories`
--

LOCK TABLES `accessories` WRITE;
/*!40000 ALTER TABLE `accessories` DISABLE KEYS */;
INSERT INTO `accessories` VALUES (1,'[Mic] Shure SM57',4,NULL),(2,'[Headphones] Beyer DT150',8,NULL),(3,'[Cable] XLR',20,NULL),(4,'[Mic] EV RE20',1,NULL),(5,'[Mic] AKG D65s',2,NULL),(6,'[Mic] Audio Technica AT4047',1,NULL),(7,'[Mic] Sennheiser MD421 II',2,NULL),(8,'[Mic] Sennheiser e609',2,NULL),(9,'[Mic] Shure SM7',2,NULL),(10,'[Mic] Shure Beta 57a',4,NULL),(11,'[Mic] Rode NTK',2,NULL),(12,'[Mic] Rode K2',2,NULL),(13,'[Mic] Rode NT5s (pair)',1,NULL),(14,'[Mic] Rode NT4',1,NULL),(15,'[Mic] Shure 588SD',1,NULL),(16,'[Mic] Shure Beta 58',2,NULL),(17,'[Mic] AKG C414 XLS',2,NULL),(18,'[Mic] AKG C414 XLS (PAIR)',1,NULL),(19,'[Mic] AKG C3000B',1,NULL),(20,'[Mic] AKG C1000S',3,NULL),(21,'[Mic] Beyerdynamic Opus 51',2,NULL),(22,'[Mic] Neumann TLM103',2,NULL),(23,'[Mic] Neumann U87',2,NULL),(24,'[Mic] Rode NT2',2,NULL),(25,'[Mic] Sennheiser EW100 (Radio Mic)',4,NULL),(26,'[Mic] Stageline ECM–925 P',1,NULL),(27,'[Mic] Rode NTG1',4,NULL),(28,'[Mic] AKG C747',1,NULL),(29,'[Mic] Sennheiser ME66',2,NULL),(30,'[Mic] Coles 4038',2,NULL),(32,'[Mic] SE Ribbon R-1',2,NULL),(34,'[Mic] Drum Mic Set - Mixed',1,NULL),(35,'[Mic] Drum Mic Set - Shure',1,NULL),(36,'[Mic] Drum Mic Set - Audio Technica',1,NULL),(37,'[Camera] Canon XA10',4,NULL),(38,'[Camera Accessory] Tripod',8,NULL),(39,'[Camera Accessory] Shoulder Mount',2,NULL),(40,'[Camera] Go Pro Hero 3',4,NULL),(41,'[Camera Accessory] Go Pro Chesty Harness',1,NULL),(42,'[Camera Accessory] Selfie Stick',1,NULL),(43,'[Camera Accessory] Floaty Backdoor',2,NULL);
/*!40000 ALTER TABLE `accessories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_activation`
--

DROP TABLE IF EXISTS `account_activation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_activation` (
  `account_activation_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `activation_code` varchar(30) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`account_activation_id`),
  UNIQUE KEY `activation_code_2` (`activation_code`),
  KEY `activation_code` (`activation_code`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `account_activation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_activation`
--

LOCK TABLES `account_activation` WRITE;
/*!40000 ALTER TABLE `account_activation` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_activation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcement_groups`
--

DROP TABLE IF EXISTS `announcement_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_groups` (
  `announcementid` mediumint(8) unsigned NOT NULL,
  `group_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`announcementid`,`group_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `announcement_groups_ibfk_1` FOREIGN KEY (`announcementid`) REFERENCES `announcements` (`announcementid`) ON DELETE CASCADE,
  CONSTRAINT `announcement_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcement_groups`
--

LOCK TABLES `announcement_groups` WRITE;
/*!40000 ALTER TABLE `announcement_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcement_resources`
--

DROP TABLE IF EXISTS `announcement_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcement_resources` (
  `announcementid` mediumint(8) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`announcementid`,`resource_id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `announcement_resources_ibfk_1` FOREIGN KEY (`announcementid`) REFERENCES `announcements` (`announcementid`) ON DELETE CASCADE,
  CONSTRAINT `announcement_resources_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcement_resources`
--

LOCK TABLES `announcement_resources` WRITE;
/*!40000 ALTER TABLE `announcement_resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcement_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `announcementid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_text` text NOT NULL,
  `priority` mediumint(8) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`announcementid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blackout_instances`
--

DROP TABLE IF EXISTS `blackout_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blackout_instances` (
  `blackout_instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `blackout_series_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`blackout_instance_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `blackout_series_id` (`blackout_series_id`),
  CONSTRAINT `blackout_instances_ibfk_1` FOREIGN KEY (`blackout_series_id`) REFERENCES `blackout_series` (`blackout_series_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blackout_instances`
--

LOCK TABLES `blackout_instances` WRITE;
/*!40000 ALTER TABLE `blackout_instances` DISABLE KEYS */;
INSERT INTO `blackout_instances` VALUES (1,'2016-12-16 17:00:00','2017-01-30 09:00:00',1),(2,'2016-12-16 17:00:00','2017-01-30 09:00:00',2);
/*!40000 ALTER TABLE `blackout_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blackout_series`
--

DROP TABLE IF EXISTS `blackout_series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blackout_series` (
  `blackout_series_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `title` varchar(85) NOT NULL,
  `description` text,
  `owner_id` mediumint(8) unsigned NOT NULL,
  `legacyid` char(16) DEFAULT NULL,
  `repeat_type` varchar(10) DEFAULT NULL,
  `repeat_options` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blackout_series_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blackout_series`
--

LOCK TABLES `blackout_series` WRITE;
/*!40000 ALTER TABLE `blackout_series` DISABLE KEYS */;
INSERT INTO `blackout_series` VALUES (1,'2016-10-11 23:30:14',NULL,'XMAS Vacation 2016',NULL,2,NULL,'none',''),(2,'2016-10-11 23:31:20',NULL,'XMAS Vacation 2016',NULL,2,NULL,'none','');
/*!40000 ALTER TABLE `blackout_series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blackout_series_resources`
--

DROP TABLE IF EXISTS `blackout_series_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blackout_series_resources` (
  `blackout_series_id` int(10) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`blackout_series_id`,`resource_id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `blackout_series_resources_ibfk_1` FOREIGN KEY (`blackout_series_id`) REFERENCES `blackout_series` (`blackout_series_id`) ON DELETE CASCADE,
  CONSTRAINT `blackout_series_resources_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blackout_series_resources`
--

LOCK TABLES `blackout_series_resources` WRITE;
/*!40000 ALTER TABLE `blackout_series_resources` DISABLE KEYS */;
INSERT INTO `blackout_series_resources` VALUES (2,3),(1,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10),(2,11),(2,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21);
/*!40000 ALTER TABLE `blackout_series_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_attribute_entities`
--

DROP TABLE IF EXISTS `custom_attribute_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_attribute_entities` (
  `custom_attribute_id` mediumint(8) unsigned NOT NULL,
  `entity_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`custom_attribute_id`,`entity_id`),
  CONSTRAINT `custom_attribute_entities_ibfk_1` FOREIGN KEY (`custom_attribute_id`) REFERENCES `custom_attributes` (`custom_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_attribute_entities`
--

LOCK TABLES `custom_attribute_entities` WRITE;
/*!40000 ALTER TABLE `custom_attribute_entities` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_attribute_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_attribute_values`
--

DROP TABLE IF EXISTS `custom_attribute_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_attribute_values` (
  `custom_attribute_value_id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `custom_attribute_id` mediumint(8) unsigned NOT NULL,
  `attribute_value` text NOT NULL,
  `entity_id` mediumint(8) unsigned NOT NULL,
  `attribute_category` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`custom_attribute_value_id`),
  KEY `custom_attribute_id` (`custom_attribute_id`),
  KEY `entity_category` (`entity_id`,`attribute_category`),
  KEY `entity_attribute` (`entity_id`,`custom_attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_attribute_values`
--

LOCK TABLES `custom_attribute_values` WRITE;
/*!40000 ALTER TABLE `custom_attribute_values` DISABLE KEYS */;
INSERT INTO `custom_attribute_values` VALUES (7,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studioX.aspx',5,4),(15,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studio1.aspx',3,4),(17,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studio3.aspx',6,4),(18,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studio4.aspx',7,4),(19,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studio5.aspx',8,4),(20,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studio_extras.aspx',9,4),(21,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/studio5b.aspx',10,4),(22,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/video_studio.aspx',11,4),(23,5,'https://vle.anglia.ac.uk/courses/amt/studios/Pages/av_lab.aspx',12,4),(24,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',4,4),(25,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',13,4),(26,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',14,4),(27,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',15,4),(28,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',16,4),(29,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',17,4),(30,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',18,4),(31,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',19,4),(32,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',20,4),(33,5,'VLE Link: https://vle.anglia.ac.uk/courses/amt/studios/Pages/equipment_loans.aspx',21,4);
/*!40000 ALTER TABLE `custom_attribute_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_attributes`
--

DROP TABLE IF EXISTS `custom_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_attributes` (
  `custom_attribute_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `display_label` varchar(50) NOT NULL,
  `display_type` tinyint(2) unsigned NOT NULL,
  `attribute_category` tinyint(2) unsigned NOT NULL,
  `validation_regex` varchar(50) DEFAULT NULL,
  `is_required` tinyint(1) unsigned NOT NULL,
  `possible_values` text,
  `sort_order` tinyint(2) unsigned DEFAULT NULL,
  `admin_only` tinyint(1) unsigned DEFAULT NULL,
  `secondary_category` tinyint(2) unsigned DEFAULT NULL,
  `secondary_entity_ids` varchar(2000) DEFAULT NULL,
  `is_private` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`custom_attribute_id`),
  KEY `attribute_category` (`attribute_category`),
  KEY `display_label` (`display_label`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_attributes`
--

LOCK TABLES `custom_attributes` WRITE;
/*!40000 ALTER TABLE `custom_attributes` DISABLE KEYS */;
INSERT INTO `custom_attributes` VALUES (5,'VLE Link',1,4,'',0,NULL,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `custom_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbversion`
--

DROP TABLE IF EXISTS `dbversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbversion` (
  `version_number` double unsigned NOT NULL DEFAULT '0',
  `version_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbversion`
--

LOCK TABLES `dbversion` WRITE;
/*!40000 ALTER TABLE `dbversion` DISABLE KEYS */;
INSERT INTO `dbversion` VALUES (2.2,'2016-10-06 16:08:11'),(2.4,'2016-10-06 16:08:11'),(2.5,'2016-10-06 16:08:11'),(2.6,'2016-11-29 15:51:53');
/*!40000 ALTER TABLE `dbversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_resource_permissions`
--

DROP TABLE IF EXISTS `group_resource_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_resource_permissions` (
  `group_id` smallint(5) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`resource_id`),
  KEY `group_id` (`group_id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `group_resource_permissions_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_resource_permissions_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_resource_permissions`
--

LOCK TABLES `group_resource_permissions` WRITE;
/*!40000 ALTER TABLE `group_resource_permissions` DISABLE KEYS */;
INSERT INTO `group_resource_permissions` VALUES (4,3),(4,4),(4,5),(4,6),(4,7),(4,8),(4,9),(4,10),(4,11),(4,12),(4,13),(4,14),(4,15),(4,16),(4,17),(4,18),(4,19),(4,20),(4,21),(6,3),(6,4),(6,5),(6,6),(6,7),(6,8),(6,9),(6,10),(6,11),(6,12),(6,13),(6,14),(6,15),(6,16),(6,17),(6,18),(6,19),(6,20),(6,21);
/*!40000 ALTER TABLE `group_resource_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_roles`
--

DROP TABLE IF EXISTS `group_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_roles` (
  `group_id` smallint(8) unsigned NOT NULL,
  `role_id` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`role_id`),
  KEY `group_id` (`group_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `group_roles_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_roles`
--

LOCK TABLES `group_roles` WRITE;
/*!40000 ALTER TABLE `group_roles` DISABLE KEYS */;
INSERT INTO `group_roles` VALUES (1,1),(2,1),(6,1),(2,2),(4,4);
/*!40000 ALTER TABLE `group_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `group_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(85) NOT NULL,
  `admin_group_id` smallint(5) unsigned DEFAULT NULL,
  `legacyid` char(16) DEFAULT NULL,
  PRIMARY KEY (`group_id`),
  KEY `admin_group_id` (`admin_group_id`),
  CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`admin_group_id`) REFERENCES `groups` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Group Administrators',NULL,NULL),(2,'Application Administrators',NULL,NULL),(3,'Resource Administrators',NULL,NULL),(4,'Schedule Administrators',NULL,NULL),(5,'Students',6,NULL),(6,'Technicians',1,NULL),(7,'Academics',6,NULL);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layouts`
--

DROP TABLE IF EXISTS `layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layouts` (
  `layout_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `timezone` varchar(50) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layouts`
--

LOCK TABLES `layouts` WRITE;
/*!40000 ALTER TABLE `layouts` DISABLE KEYS */;
INSERT INTO `layouts` VALUES (2,'Europe/London'),(4,'Europe/London');
/*!40000 ALTER TABLE `layouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peak_times`
--

DROP TABLE IF EXISTS `peak_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peak_times` (
  `peak_times_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` smallint(5) unsigned NOT NULL,
  `all_day` tinyint(1) unsigned NOT NULL,
  `start_time` varchar(10) DEFAULT NULL,
  `end_time` varchar(10) DEFAULT NULL,
  `every_day` tinyint(1) unsigned NOT NULL,
  `peak_days` varchar(13) DEFAULT NULL,
  `all_year` tinyint(1) unsigned NOT NULL,
  `begin_month` tinyint(1) unsigned NOT NULL,
  `begin_day` tinyint(1) unsigned NOT NULL,
  `end_month` tinyint(1) unsigned NOT NULL,
  `end_day` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`peak_times_id`),
  KEY `schedule_id` (`schedule_id`),
  CONSTRAINT `peak_times_ibfk_1` FOREIGN KEY (`schedule_id`) REFERENCES `schedules` (`schedule_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peak_times`
--

LOCK TABLES `peak_times` WRITE;
/*!40000 ALTER TABLE `peak_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `peak_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotas`
--

DROP TABLE IF EXISTS `quotas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotas` (
  `quota_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `quota_limit` decimal(7,2) unsigned NOT NULL,
  `unit` varchar(25) NOT NULL,
  `duration` varchar(25) NOT NULL,
  `resource_id` smallint(5) unsigned DEFAULT NULL,
  `group_id` smallint(5) unsigned DEFAULT NULL,
  `schedule_id` smallint(5) unsigned DEFAULT NULL,
  `enforced_days` varchar(15) DEFAULT NULL,
  `enforced_time_start` time DEFAULT NULL,
  `enforced_time_end` time DEFAULT NULL,
  `scope` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`quota_id`),
  KEY `resource_id` (`resource_id`),
  KEY `group_id` (`group_id`),
  KEY `schedule_id` (`schedule_id`),
  CONSTRAINT `quotas_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quotas_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `quotas_ibfk_3` FOREIGN KEY (`schedule_id`) REFERENCES `schedules` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotas`
--

LOCK TABLES `quotas` WRITE;
/*!40000 ALTER TABLE `quotas` DISABLE KEYS */;
INSERT INTO `quotas` VALUES (1,0.00,'hours','day',NULL,5,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quotas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `reminder_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `address` text NOT NULL,
  `message` text NOT NULL,
  `sendtime` datetime NOT NULL,
  `refnumber` text NOT NULL,
  PRIMARY KEY (`reminder_id`),
  KEY `reminders_user_id` (`user_id`),
  CONSTRAINT `reminders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_accessories`
--

DROP TABLE IF EXISTS `reservation_accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_accessories` (
  `series_id` int(10) unsigned NOT NULL,
  `accessory_id` smallint(5) unsigned NOT NULL,
  `quantity` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`series_id`,`accessory_id`),
  KEY `accessory_id` (`accessory_id`),
  KEY `series_id` (`series_id`),
  CONSTRAINT `reservation_accessories_ibfk_1` FOREIGN KEY (`accessory_id`) REFERENCES `accessories` (`accessory_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reservation_accessories_ibfk_2` FOREIGN KEY (`series_id`) REFERENCES `reservation_series` (`series_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_accessories`
--

LOCK TABLES `reservation_accessories` WRITE;
/*!40000 ALTER TABLE `reservation_accessories` DISABLE KEYS */;
INSERT INTO `reservation_accessories` VALUES (1,1,4),(3,4,1),(5,1,2),(5,2,4),(5,4,1),(7,1,2),(7,2,4),(7,4,1),(7,13,1),(9,6,1),(9,13,1),(9,14,1),(9,18,1),(9,19,1),(9,28,1),(10,4,1),(10,34,1),(10,35,1),(12,14,1),(12,29,2),(12,34,1),(13,13,1),(14,3,12),(14,16,1),(14,20,2),(14,28,1),(14,37,2),(14,38,1),(14,39,1),(14,40,1),(14,41,1),(15,2,2),(15,7,2),(15,10,4),(15,12,2),(15,32,2),(15,36,1),(16,3,2),(16,21,2),(16,37,3),(16,38,2),(16,39,1),(16,40,1),(16,41,1),(17,5,2),(17,22,2),(17,23,2),(17,32,2),(18,15,1),(18,18,1),(18,28,1),(18,36,1),(18,41,1),(18,42,1),(19,22,2),(19,24,2),(19,26,1),(19,29,1),(19,38,3),(21,34,1),(21,35,1),(21,36,1),(23,2,1),(23,12,2),(23,23,2),(23,36,1),(23,37,2),(23,38,1),(23,39,1),(23,40,1),(23,41,1),(23,42,1),(24,3,6),(25,12,2),(26,9,1),(26,25,2),(26,40,1),(27,10,1),(27,20,1),(27,39,1),(27,43,1),(30,2,2),(30,7,2),(30,32,2),(30,36,1),(30,37,2),(30,38,1),(30,39,1),(30,40,1),(30,41,1),(36,36,1),(38,3,5),(39,13,1),(39,14,1),(39,42,1),(40,9,2),(40,10,2),(40,13,1);
/*!40000 ALTER TABLE `reservation_accessories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_color_rules`
--

DROP TABLE IF EXISTS `reservation_color_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_color_rules` (
  `reservation_color_rule_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `custom_attribute_id` mediumint(8) unsigned NOT NULL,
  `attribute_type` smallint(5) unsigned DEFAULT NULL,
  `required_value` text,
  `comparison_type` smallint(5) unsigned DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`reservation_color_rule_id`),
  KEY `custom_attribute_id` (`custom_attribute_id`),
  CONSTRAINT `reservation_color_rules_ibfk_1` FOREIGN KEY (`custom_attribute_id`) REFERENCES `custom_attributes` (`custom_attribute_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_color_rules`
--

LOCK TABLES `reservation_color_rules` WRITE;
/*!40000 ALTER TABLE `reservation_color_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_color_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_files`
--

DROP TABLE IF EXISTS `reservation_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `series_id` int(10) unsigned NOT NULL,
  `file_name` varchar(250) NOT NULL,
  `file_type` varchar(75) DEFAULT NULL,
  `file_size` varchar(45) NOT NULL,
  `file_extension` varchar(10) NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `series_id` (`series_id`),
  CONSTRAINT `reservation_files_ibfk_1` FOREIGN KEY (`series_id`) REFERENCES `reservation_series` (`series_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_files`
--

LOCK TABLES `reservation_files` WRITE;
/*!40000 ALTER TABLE `reservation_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_guests`
--

DROP TABLE IF EXISTS `reservation_guests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_guests` (
  `reservation_instance_id` int(10) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `reservation_user_level` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`reservation_instance_id`,`email`),
  KEY `reservation_guests_reservation_instance_id` (`reservation_instance_id`),
  KEY `reservation_guests_email_address` (`email`),
  KEY `reservation_guests_reservation_user_level` (`reservation_user_level`),
  CONSTRAINT `reservation_guests_ibfk_1` FOREIGN KEY (`reservation_instance_id`) REFERENCES `reservation_instances` (`reservation_instance_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_guests`
--

LOCK TABLES `reservation_guests` WRITE;
/*!40000 ALTER TABLE `reservation_guests` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_guests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_instances`
--

DROP TABLE IF EXISTS `reservation_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_instances` (
  `reservation_instance_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `reference_number` varchar(50) NOT NULL,
  `series_id` int(10) unsigned NOT NULL,
  `checkin_date` datetime DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `previous_end_date` datetime DEFAULT NULL,
  `credit_count` decimal(7,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`reservation_instance_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `reference_number` (`reference_number`),
  KEY `series_id` (`series_id`),
  KEY `checkin_date` (`checkin_date`),
  CONSTRAINT `reservations_series` FOREIGN KEY (`series_id`) REFERENCES `reservation_series` (`series_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_instances`
--

LOCK TABLES `reservation_instances` WRITE;
/*!40000 ALTER TABLE `reservation_instances` DISABLE KEYS */;
INSERT INTO `reservation_instances` VALUES (1,'2016-10-07 15:00:00','2016-10-07 16:00:00','57f7ad02b2393628031840',1,NULL,NULL,NULL,NULL),(2,'2016-10-07 15:00:00','2016-10-07 16:00:00','57f7b84572c18652595762',2,NULL,NULL,NULL,NULL),(3,'2016-10-11 08:00:00','2016-10-11 09:00:00','57fb7e903ca76246090563',3,NULL,NULL,NULL,NULL),(4,'2016-10-10 13:00:00','2016-10-10 16:00:00','57fb8342c33e5890007530',4,NULL,NULL,NULL,NULL),(5,'2016-10-10 13:00:00','2016-10-10 15:00:00','57fb8a8f88e94178736170',5,NULL,NULL,NULL,NULL),(6,'2016-10-12 09:00:00','2016-10-12 10:00:00','57fd5bc014051237466105',6,NULL,NULL,NULL,NULL),(7,'2016-10-12 13:00:00','2016-10-12 16:00:00','57fd60039e1f1117181173',7,NULL,NULL,NULL,NULL),(8,'2016-10-12 12:00:00','2016-10-12 13:00:00','57fd7ce418742619462429',8,NULL,NULL,NULL,NULL),(9,'2016-10-13 13:00:00','2016-10-13 16:00:00','57fd82eb0e572486385210',9,NULL,NULL,NULL,NULL),(10,'2016-10-12 10:00:00','2016-10-12 12:00:00','57fd85eb40b0f981193428',10,NULL,NULL,NULL,NULL),(11,'2016-10-12 10:00:00','2016-10-12 11:00:00','57fd8618726fa874721543',11,NULL,NULL,NULL,NULL),(12,'2016-10-14 08:00:00','2016-10-17 09:00:00','57ffad248b907942396176',12,NULL,NULL,NULL,NULL),(13,'2016-10-17 15:00:00','2016-10-18 08:00:00','57ffb11a14178445873227',13,NULL,NULL,NULL,NULL),(14,'2016-10-14 15:00:00','2016-10-17 09:00:00','5800ec1edb493247754339',14,NULL,NULL,NULL,NULL),(15,'2016-10-17 08:00:00','2016-10-17 16:00:00','5803f2480c200516637671',15,NULL,NULL,NULL,NULL),(16,'2016-10-17 15:00:00','2016-10-18 08:00:00','580400556cb56519782850',16,NULL,NULL,NULL,NULL),(17,'2016-10-19 15:00:00','2016-10-20 09:00:00','580401505e3c5257533383',17,NULL,NULL,NULL,NULL),(18,'2016-10-19 15:00:00','2016-10-20 08:00:00','5805dc9a9116f691520727',18,NULL,NULL,NULL,NULL),(19,'2016-10-18 15:00:00','2016-10-19 08:00:00','580614017028f303707482',19,NULL,NULL,NULL,NULL),(20,'2016-10-17 15:00:00','2016-10-17 16:00:00','580614d1d0aed024035818',20,NULL,NULL,NULL,NULL),(21,'2016-10-18 13:00:00','2016-10-18 16:00:00','5806175283de9023259750',21,NULL,NULL,NULL,NULL),(23,'2016-10-19 13:00:00','2016-10-19 16:00:00','580621268e7a4166466924',23,NULL,NULL,NULL,NULL),(24,'2016-10-19 15:00:00','2016-10-19 16:00:00','58062aa5ead86229545521',24,NULL,NULL,NULL,NULL),(25,'2016-10-19 14:00:00','2016-10-19 16:00:00','58062b4c23ffa923923343',25,NULL,NULL,NULL,NULL),(26,'2016-10-19 13:00:00','2016-10-19 14:00:00','58062bfc7712b265594470',26,NULL,NULL,NULL,NULL),(27,'2016-10-19 11:00:00','2016-10-19 16:00:00','58062cd5a926e913974619',27,NULL,NULL,NULL,NULL),(28,'2016-10-19 13:00:00','2016-10-19 16:00:00','58062d4789cc5556847358',28,NULL,NULL,NULL,NULL),(29,'2016-10-19 10:00:00','2016-10-19 15:00:00','58073fa3ed7db387481635',29,NULL,NULL,NULL,NULL),(30,'2016-10-19 11:00:00','2016-10-19 16:00:00','580750499724b906169341',30,NULL,NULL,NULL,NULL),(31,'2016-10-19 14:00:00','2016-10-19 15:00:00','58077727dc685817154244',31,NULL,NULL,NULL,NULL),(32,'2016-10-19 12:00:00','2016-10-19 15:00:00','58078926483a1666763579',32,NULL,NULL,NULL,NULL),(33,'2016-10-20 09:00:00','2016-10-20 10:00:00','580885f8e0b38469743765',33,NULL,NULL,NULL,NULL),(34,'2016-10-20 09:00:00','2016-10-20 10:00:00','58088605470a6834246160',34,NULL,NULL,NULL,NULL),(35,'2016-10-21 12:00:00','2016-10-21 13:00:00','5809ff5d88cc0245453417',35,NULL,NULL,NULL,NULL),(36,'2016-10-21 13:00:00','2016-10-21 14:00:00','580a0b54a5dc1580067307',36,NULL,NULL,NULL,NULL),(37,'2016-10-21 14:00:00','2016-10-21 15:00:00','580a120f29659439692157',37,NULL,NULL,NULL,NULL),(38,'2016-11-29 16:00:00','2016-11-29 17:00:00','583da502c1943713744962',38,NULL,NULL,NULL,NULL),(39,'2016-11-30 09:00:00','2016-11-30 11:00:00','583db72f1cbe7290968832',39,'2016-11-30 09:59:03','2016-11-30 10:22:10','2016-11-30 10:22:10',NULL),(40,'2016-11-30 11:00:00','2016-11-30 12:00:00','583ea6189f31e163701645',40,NULL,NULL,NULL,NULL),(41,'2016-11-30 12:00:00','2016-11-30 14:00:00','583ebf2a179a1264881045',41,'2016-11-30 12:02:20',NULL,NULL,NULL),(42,'2017-03-20 09:00:00','2017-03-20 10:00:00','58cc1bbc59db9789266784',42,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `reservation_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_reminders`
--

DROP TABLE IF EXISTS `reservation_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_reminders` (
  `reminder_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `series_id` int(10) unsigned NOT NULL,
  `minutes_prior` int(10) unsigned NOT NULL,
  `reminder_type` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`reminder_id`),
  KEY `series_id` (`series_id`),
  CONSTRAINT `reservation_reminders_ibfk_1` FOREIGN KEY (`series_id`) REFERENCES `reservation_series` (`series_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_reminders`
--

LOCK TABLES `reservation_reminders` WRITE;
/*!40000 ALTER TABLE `reservation_reminders` DISABLE KEYS */;
INSERT INTO `reservation_reminders` VALUES (2,1,135,1),(3,1,45,0),(4,35,18,0),(5,40,43,0);
/*!40000 ALTER TABLE `reservation_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_resources`
--

DROP TABLE IF EXISTS `reservation_resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_resources` (
  `series_id` int(10) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  `resource_level_id` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`series_id`,`resource_id`),
  KEY `resource_id` (`resource_id`),
  KEY `series_id` (`series_id`),
  CONSTRAINT `reservation_resources_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reservation_resources_ibfk_2` FOREIGN KEY (`series_id`) REFERENCES `reservation_series` (`series_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_resources`
--

LOCK TABLES `reservation_resources` WRITE;
/*!40000 ALTER TABLE `reservation_resources` DISABLE KEYS */;
INSERT INTO `reservation_resources` VALUES (1,3,1),(2,5,1),(3,5,1),(3,9,2),(4,6,1),(5,6,1),(5,8,2),(5,9,2),(6,10,1),(7,8,1),(7,9,2),(8,10,1),(9,7,1),(10,3,1),(11,5,1),(12,4,1),(13,4,1),(14,15,1),(15,9,1),(16,13,1),(17,16,1),(18,15,1),(19,4,1),(20,11,1),(21,8,1),(23,8,1),(24,7,1),(25,10,1),(26,5,1),(27,10,1),(28,7,1),(29,3,1),(30,9,1),(30,10,2),(31,5,1),(32,6,1),(33,8,1),(33,9,2),(34,5,1),(35,3,1),(36,3,1),(37,3,1),(38,3,1),(39,3,1),(40,5,1),(41,5,1),(42,3,1);
/*!40000 ALTER TABLE `reservation_resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_series`
--

DROP TABLE IF EXISTS `reservation_series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_series` (
  `series_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `title` varchar(85) NOT NULL,
  `description` text,
  `allow_participation` tinyint(1) unsigned NOT NULL,
  `allow_anon_participation` tinyint(1) unsigned NOT NULL,
  `type_id` tinyint(2) unsigned NOT NULL,
  `status_id` tinyint(2) unsigned NOT NULL,
  `repeat_type` varchar(10) DEFAULT NULL,
  `repeat_options` varchar(255) DEFAULT NULL,
  `owner_id` mediumint(8) unsigned NOT NULL,
  `legacyid` char(16) DEFAULT NULL,
  `last_action_by` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`series_id`),
  KEY `type_id` (`type_id`),
  KEY `status_id` (`status_id`),
  KEY `reservations_owner` (`owner_id`),
  CONSTRAINT `reservations_owner` FOREIGN KEY (`owner_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `reservations_status` FOREIGN KEY (`status_id`) REFERENCES `reservation_statuses` (`status_id`) ON UPDATE CASCADE,
  CONSTRAINT `reservations_type` FOREIGN KEY (`type_id`) REFERENCES `reservation_types` (`type_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_series`
--

LOCK TABLES `reservation_series` WRITE;
/*!40000 ALTER TABLE `reservation_series` DISABLE KEYS */;
INSERT INTO `reservation_series` VALUES (1,'2016-10-07 14:11:14','2016-10-07 14:50:55','','',0,0,1,1,'none','',3,NULL,NULL),(2,'2016-10-07 14:59:17',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(3,'2016-10-10 11:42:08',NULL,'','',0,0,1,1,'none','',3,NULL,NULL),(4,'2016-10-10 12:02:10','2016-10-10 12:09:58','','',0,0,1,2,'none','',5,NULL,NULL),(5,'2016-10-10 12:33:19',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(6,'2016-10-11 21:38:08',NULL,'Test','',0,0,1,1,'none','',3,NULL,NULL),(7,'2016-10-11 21:56:19',NULL,'','',0,0,1,1,'none','',3,NULL,NULL),(8,'2016-10-11 23:59:32',NULL,'','',0,0,1,1,'none','',7,NULL,NULL),(9,'2016-10-12 00:25:15',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(10,'2016-10-12 00:38:03',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(11,'2016-10-12 00:38:48',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(12,'2016-10-13 15:49:56','2016-10-16 19:34:42','','',0,0,1,1,'none','',5,NULL,NULL),(13,'2016-10-13 16:06:50',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(14,'2016-10-14 14:30:54','2016-10-16 21:22:36','','',0,0,1,1,'none','',5,NULL,NULL),(15,'2016-10-16 21:34:00',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(16,'2016-10-16 22:33:57',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(17,'2016-10-16 22:38:08','2016-10-18 13:26:43','','',0,0,1,2,'none','',5,NULL,NULL),(18,'2016-10-18 08:26:02',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(19,'2016-10-18 12:22:25',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(20,'2016-10-18 12:25:53',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(21,'2016-10-18 12:36:34',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(23,'2016-10-18 13:18:30','2016-10-18 13:48:46','','',0,0,1,2,'none','',10,NULL,NULL),(24,'2016-10-18 13:59:02','2016-10-18 13:59:39','','',0,0,1,2,'none','',5,NULL,NULL),(25,'2016-10-18 14:01:48','2016-10-18 14:03:17','','',0,0,1,2,'none','',5,NULL,NULL),(26,'2016-10-18 14:04:44','2016-10-18 14:05:03','','',0,0,1,2,'none','',5,NULL,NULL),(27,'2016-10-18 14:08:21','2016-10-18 14:08:50','','',0,0,1,2,'none','',5,NULL,NULL),(28,'2016-10-18 14:10:15','2016-10-18 14:10:29','','',0,0,1,2,'none','',5,NULL,NULL),(29,'2016-10-19 09:40:52',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(30,'2016-10-19 10:51:53',NULL,'','',0,0,1,1,'none','',9,NULL,NULL),(31,'2016-10-19 13:37:43',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(32,'2016-10-19 14:54:30',NULL,'','',0,0,1,1,'none','',3,NULL,NULL),(33,'2016-10-20 08:53:12',NULL,'','',0,0,1,1,'none','',3,NULL,NULL),(34,'2016-10-20 08:53:25',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(35,'2016-10-21 11:43:25',NULL,'','',0,0,1,1,'none','',3,NULL,NULL),(36,'2016-10-21 12:34:28','2016-10-21 12:58:13','','',0,0,1,1,'none','',3,NULL,NULL),(37,'2016-10-21 13:03:11','2016-10-21 13:10:15','','',0,0,1,2,'none','',3,NULL,NULL),(38,'2016-11-29 15:55:46',NULL,'','',0,0,1,1,'none','',5,NULL,NULL),(39,'2016-11-29 17:13:19','2016-11-30 10:03:17','','',0,0,1,2,'none','',3,NULL,NULL),(40,'2016-11-30 10:12:40','2016-11-30 10:14:44','','',0,0,1,1,'none','',3,NULL,NULL),(41,'2016-11-30 11:59:38','2016-11-30 12:02:20','','',0,0,1,1,'none','',3,NULL,NULL),(42,'2017-03-17 17:24:12',NULL,'','',0,0,1,1,'none','',3,NULL,NULL);
/*!40000 ALTER TABLE `reservation_series` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_statuses`
--

DROP TABLE IF EXISTS `reservation_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_statuses` (
  `status_id` tinyint(2) unsigned NOT NULL,
  `label` varchar(85) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_statuses`
--

LOCK TABLES `reservation_statuses` WRITE;
/*!40000 ALTER TABLE `reservation_statuses` DISABLE KEYS */;
INSERT INTO `reservation_statuses` VALUES (1,'Created'),(2,'Deleted'),(3,'Pending');
/*!40000 ALTER TABLE `reservation_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_types`
--

DROP TABLE IF EXISTS `reservation_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_types` (
  `type_id` tinyint(2) unsigned NOT NULL,
  `label` varchar(85) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_types`
--

LOCK TABLES `reservation_types` WRITE;
/*!40000 ALTER TABLE `reservation_types` DISABLE KEYS */;
INSERT INTO `reservation_types` VALUES (1,'Reservation'),(2,'Blackout');
/*!40000 ALTER TABLE `reservation_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_users`
--

DROP TABLE IF EXISTS `reservation_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_users` (
  `reservation_instance_id` int(10) unsigned NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  `reservation_user_level` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`reservation_instance_id`,`user_id`),
  KEY `reservation_instance_id` (`reservation_instance_id`),
  KEY `user_id` (`user_id`),
  KEY `reservation_user_level` (`reservation_user_level`),
  CONSTRAINT `reservation_users_ibfk_1` FOREIGN KEY (`reservation_instance_id`) REFERENCES `reservation_instances` (`reservation_instance_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reservation_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_users`
--

LOCK TABLES `reservation_users` WRITE;
/*!40000 ALTER TABLE `reservation_users` DISABLE KEYS */;
INSERT INTO `reservation_users` VALUES (1,3,1),(2,5,1),(3,3,1),(4,5,1),(5,5,1),(6,3,1),(7,3,1),(8,7,1),(9,5,1),(10,5,1),(11,5,1),(12,5,1),(13,5,1),(14,5,1),(15,5,1),(16,5,1),(17,5,1),(18,5,1),(19,5,1),(20,5,1),(21,5,1),(23,10,1),(24,5,1),(25,5,1),(26,5,1),(27,5,1),(28,5,1),(29,5,1),(30,9,1),(31,5,1),(32,3,1),(33,3,1),(34,5,1),(35,3,1),(36,3,1),(37,3,1),(38,5,1),(39,3,1),(40,3,1),(41,3,1),(42,3,1),(4,2,2),(4,3,2);
/*!40000 ALTER TABLE `reservation_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_waitlist_requests`
--

DROP TABLE IF EXISTS `reservation_waitlist_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_waitlist_requests` (
  `reservation_waitlist_request_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`reservation_waitlist_request_id`),
  KEY `user_id` (`user_id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `reservation_waitlist_requests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `reservation_waitlist_requests_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_waitlist_requests`
--

LOCK TABLES `reservation_waitlist_requests` WRITE;
/*!40000 ALTER TABLE `reservation_waitlist_requests` DISABLE KEYS */;
INSERT INTO `reservation_waitlist_requests` VALUES (1,5,3,'2016-11-30 09:00:00','2016-11-30 11:00:00');
/*!40000 ALTER TABLE `reservation_waitlist_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_accessories`
--

DROP TABLE IF EXISTS `resource_accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_accessories` (
  `resource_accessory_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` smallint(5) unsigned NOT NULL,
  `accessory_id` smallint(5) unsigned NOT NULL,
  `minimum_quantity` smallint(6) DEFAULT NULL,
  `maximum_quantity` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`resource_accessory_id`),
  KEY `resource_id` (`resource_id`),
  KEY `accessory_id` (`accessory_id`),
  CONSTRAINT `resource_accessories_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE,
  CONSTRAINT `resource_accessories_ibfk_2` FOREIGN KEY (`accessory_id`) REFERENCES `accessories` (`accessory_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_accessories`
--

LOCK TABLES `resource_accessories` WRITE;
/*!40000 ALTER TABLE `resource_accessories` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_accessories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_group_assignment`
--

DROP TABLE IF EXISTS `resource_group_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_group_assignment` (
  `resource_group_id` mediumint(8) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`resource_group_id`,`resource_id`),
  KEY `resource_group_assignment_resource_id` (`resource_id`),
  KEY `resource_group_assignment_resource_group_id` (`resource_group_id`),
  CONSTRAINT `resource_group_assignment_ibfk_1` FOREIGN KEY (`resource_group_id`) REFERENCES `resource_groups` (`resource_group_id`) ON DELETE CASCADE,
  CONSTRAINT `resource_group_assignment_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_group_assignment`
--

LOCK TABLES `resource_group_assignment` WRITE;
/*!40000 ALTER TABLE `resource_group_assignment` DISABLE KEYS */;
INSERT INTO `resource_group_assignment` VALUES (1,3),(4,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(4,13),(4,14),(4,15),(4,16),(4,17),(4,18),(4,19),(4,20),(4,21);
/*!40000 ALTER TABLE `resource_group_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_groups`
--

DROP TABLE IF EXISTS `resource_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_groups` (
  `resource_group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `resource_group_name` varchar(75) DEFAULT NULL,
  `parent_id` mediumint(8) unsigned DEFAULT NULL,
  `public_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`resource_group_id`),
  KEY `resource_groups_parent_id` (`parent_id`),
  CONSTRAINT `resource_groups_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `resource_groups` (`resource_group_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_groups`
--

LOCK TABLES `resource_groups` WRITE;
/*!40000 ALTER TABLE `resource_groups` DISABLE KEYS */;
INSERT INTO `resource_groups` VALUES (1,'Studios',NULL,NULL),(4,'Equipment Loans',NULL,NULL);
/*!40000 ALTER TABLE `resource_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_status_reasons`
--

DROP TABLE IF EXISTS `resource_status_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_status_reasons` (
  `resource_status_reason_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `status_id` tinyint(3) unsigned NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`resource_status_reason_id`),
  KEY `status_id` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_status_reasons`
--

LOCK TABLES `resource_status_reasons` WRITE;
/*!40000 ALTER TABLE `resource_status_reasons` DISABLE KEYS */;
INSERT INTO `resource_status_reasons` VALUES (1,2,'Technical Issues'),(2,1,'Bookable');
/*!40000 ALTER TABLE `resource_status_reasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_type_assignment`
--

DROP TABLE IF EXISTS `resource_type_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_type_assignment` (
  `resource_id` smallint(5) unsigned NOT NULL,
  `resource_type_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`resource_id`,`resource_type_id`),
  KEY `resource_type_id` (`resource_type_id`),
  CONSTRAINT `resource_type_assignment_ibfk_1` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE,
  CONSTRAINT `resource_type_assignment_ibfk_2` FOREIGN KEY (`resource_type_id`) REFERENCES `resource_types` (`resource_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_type_assignment`
--

LOCK TABLES `resource_type_assignment` WRITE;
/*!40000 ALTER TABLE `resource_type_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_type_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_types`
--

DROP TABLE IF EXISTS `resource_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_types` (
  `resource_type_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `resource_type_name` varchar(75) DEFAULT NULL,
  `resource_type_description` text,
  PRIMARY KEY (`resource_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_types`
--

LOCK TABLES `resource_types` WRITE;
/*!40000 ALTER TABLE `resource_types` DISABLE KEYS */;
INSERT INTO `resource_types` VALUES (1,'Studio Room',''),(2,'Equipment Loan','');
/*!40000 ALTER TABLE `resource_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `resource_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(85) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `contact_info` varchar(255) DEFAULT NULL,
  `description` text,
  `notes` text,
  `min_duration` int(11) DEFAULT NULL,
  `min_increment` int(11) DEFAULT NULL,
  `max_duration` int(11) DEFAULT NULL,
  `unit_cost` decimal(7,2) DEFAULT NULL,
  `autoassign` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `requires_approval` tinyint(1) unsigned NOT NULL,
  `allow_multiday_reservations` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `max_participants` mediumint(8) unsigned DEFAULT NULL,
  `min_notice_time` int(11) DEFAULT NULL,
  `max_notice_time` int(11) DEFAULT NULL,
  `image_name` varchar(50) DEFAULT NULL,
  `schedule_id` smallint(5) unsigned NOT NULL,
  `legacyid` char(16) DEFAULT NULL,
  `admin_group_id` smallint(5) unsigned DEFAULT NULL,
  `public_id` varchar(20) DEFAULT NULL,
  `allow_calendar_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `sort_order` tinyint(2) unsigned DEFAULT NULL,
  `resource_type_id` mediumint(8) unsigned DEFAULT NULL,
  `status_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `resource_status_reason_id` smallint(5) unsigned DEFAULT NULL,
  `buffer_time` int(10) unsigned DEFAULT NULL,
  `enable_check_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `auto_release_minutes` smallint(5) unsigned DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `allow_display` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `credit_count` decimal(7,2) unsigned DEFAULT NULL,
  `peak_credit_count` decimal(7,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`resource_id`),
  UNIQUE KEY `public_id` (`public_id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `admin_group_id` (`admin_group_id`),
  KEY `resource_type_id` (`resource_type_id`),
  KEY `resource_status_reason_id` (`resource_status_reason_id`),
  KEY `auto_release_minutes` (`auto_release_minutes`),
  CONSTRAINT `admin_group_id` FOREIGN KEY (`admin_group_id`) REFERENCES `groups` (`group_id`) ON DELETE SET NULL,
  CONSTRAINT `resources_ibfk_1` FOREIGN KEY (`schedule_id`) REFERENCES `schedules` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `resources_ibfk_2` FOREIGN KEY (`resource_type_id`) REFERENCES `resource_types` (`resource_type_id`) ON DELETE SET NULL,
  CONSTRAINT `resources_ibfk_3` FOREIGN KEY (`resource_status_reason_id`) REFERENCES `resource_status_reasons` (`resource_status_reason_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
INSERT INTO `resources` VALUES (3,'Studio 1','COM120','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,0,1,1,2,NULL,1,NULL,'',0,0.00,0.00),(4,'Loan Slot 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,6,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(5,'Studio 2','COM117','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,1,1,1,2,NULL,1,NULL,'',0,0.00,0.00),(6,'Studio 3','COM115','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,2,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(7,'Studio 4','COM113','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,3,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(8,'Studio 5','COM112','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,4,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(9,'Studio 5 Live Room','COM111','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,5,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(10,'Studio 5b','Com110','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,6,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(11,'Video Studio','COM102','',NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,7,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(12,'AV Lab',NULL,NULL,NULL,NULL,3600,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,10,1,1,2,NULL,0,NULL,NULL,0,NULL,NULL),(13,'Loan Slot 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(14,'Loan Slot 3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(15,'Loan Slot 4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(16,'Loan Slot 5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(17,'Loan Slot 6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(18,'Loan Slot 7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(19,'Loan Slot 8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(20,'Loan Slot 9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL),(21,'Loan Slot 10',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,0,0,2,1,NULL,NULL,0,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `role_id` tinyint(2) unsigned NOT NULL,
  `name` varchar(85) DEFAULT NULL,
  `role_level` tinyint(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Group Admin',1),(2,'Application Admin',2),(3,'Resource Admin',3),(4,'Schedule Admin',4);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_reports`
--

DROP TABLE IF EXISTS `saved_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_reports` (
  `saved_report_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `report_name` varchar(50) DEFAULT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `report_details` varchar(500) NOT NULL,
  PRIMARY KEY (`saved_report_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `saved_reports_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_reports`
--

LOCK TABLES `saved_reports` WRITE;
/*!40000 ALTER TABLE `saved_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `schedule_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(85) NOT NULL,
  `isdefault` tinyint(1) unsigned NOT NULL,
  `weekdaystart` tinyint(2) unsigned NOT NULL,
  `daysvisible` tinyint(2) unsigned NOT NULL DEFAULT '7',
  `layout_id` mediumint(8) unsigned NOT NULL,
  `legacyid` char(16) DEFAULT NULL,
  `public_id` varchar(20) DEFAULT NULL,
  `allow_calendar_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `admin_group_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`schedule_id`),
  UNIQUE KEY `public_id` (`public_id`),
  KEY `layout_id` (`layout_id`),
  KEY `schedules_groups_admin_group_id` (`admin_group_id`),
  CONSTRAINT `schedules_groups_admin_group_id` FOREIGN KEY (`admin_group_id`) REFERENCES `groups` (`group_id`) ON DELETE SET NULL,
  CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`layout_id`) REFERENCES `layouts` (`layout_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (1,'Studios',1,1,5,4,NULL,NULL,0,6),(3,'Equipment Loans',0,100,7,2,NULL,NULL,0,6);
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `time_blocks`
--

DROP TABLE IF EXISTS `time_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_blocks` (
  `block_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(85) DEFAULT NULL,
  `end_label` varchar(85) DEFAULT NULL,
  `availability_code` tinyint(2) unsigned NOT NULL,
  `layout_id` mediumint(8) unsigned NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `day_of_week` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`block_id`),
  KEY `layout_id` (`layout_id`),
  CONSTRAINT `time_blocks_ibfk_1` FOREIGN KEY (`layout_id`) REFERENCES `layouts` (`layout_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `time_blocks`
--

LOCK TABLES `time_blocks` WRITE;
/*!40000 ALTER TABLE `time_blocks` DISABLE KEYS */;
INSERT INTO `time_blocks` VALUES (23,NULL,NULL,2,2,'00:00:00','09:00:00',NULL),(24,NULL,NULL,1,2,'09:00:00','10:00:00',NULL),(25,NULL,NULL,1,2,'10:00:00','11:00:00',NULL),(26,NULL,NULL,1,2,'11:00:00','12:00:00',NULL),(27,NULL,NULL,1,2,'12:00:00','13:00:00',NULL),(28,NULL,NULL,1,2,'13:00:00','14:00:00',NULL),(29,NULL,NULL,1,2,'14:00:00','15:00:00',NULL),(30,NULL,NULL,1,2,'15:00:00','16:00:00',NULL),(31,NULL,NULL,1,2,'16:00:00','17:00:00',NULL),(32,NULL,NULL,2,2,'17:00:00','00:00:00',NULL),(51,NULL,NULL,2,4,'00:00:00','09:00:00',NULL),(52,NULL,NULL,1,4,'09:00:00','10:00:00',NULL),(53,NULL,NULL,1,4,'10:00:00','11:00:00',NULL),(54,NULL,NULL,1,4,'11:00:00','12:00:00',NULL),(55,NULL,NULL,1,4,'12:00:00','13:00:00',NULL),(56,NULL,NULL,1,4,'13:00:00','14:00:00',NULL),(57,NULL,NULL,1,4,'14:00:00','15:00:00',NULL),(58,NULL,NULL,1,4,'15:00:00','16:00:00',NULL),(59,NULL,NULL,1,4,'16:00:00','17:00:00',NULL),(60,NULL,NULL,2,4,'17:00:00','00:00:00',NULL);
/*!40000 ALTER TABLE `time_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_email_preferences`
--

DROP TABLE IF EXISTS `user_email_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_email_preferences` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `event_category` varchar(45) NOT NULL,
  `event_type` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`,`event_category`,`event_type`),
  CONSTRAINT `user_email_preferences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_email_preferences`
--

LOCK TABLES `user_email_preferences` WRITE;
/*!40000 ALTER TABLE `user_email_preferences` DISABLE KEYS */;
INSERT INTO `user_email_preferences` VALUES (3,'reservation','approved'),(3,'reservation','created'),(3,'reservation','deleted'),(3,'reservation','updated'),(5,'reservation','approved'),(5,'reservation','created'),(5,'reservation','deleted'),(5,'reservation','updated'),(7,'reservation','approved'),(7,'reservation','created'),(7,'reservation','deleted'),(7,'reservation','updated'),(9,'reservation','approved'),(9,'reservation','created'),(9,'reservation','deleted'),(9,'reservation','updated'),(10,'reservation','approved'),(10,'reservation','created'),(10,'reservation','deleted'),(10,'reservation','updated');
/*!40000 ALTER TABLE `user_email_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `user_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (3,1),(2,2),(3,3),(5,5),(3,6),(7,6),(9,6),(10,6);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `user_preferences_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`user_preferences_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_preferences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
INSERT INTO `user_preferences` VALUES (1,7,'FilterStartDateDelta','-14'),(2,7,'FilterEndDateDelta','7'),(3,7,'FilterUserId',''),(4,7,'FilterUserName',''),(5,7,'FilterScheduleId','0'),(6,7,'FilterResourceId','0'),(7,7,'FilterReservationStatusId','0'),(8,7,'FilterReferenceNumber',''),(9,7,'FilterResourceStatusId',''),(10,7,'FilterResourceReasonId',''),(11,7,'FilterCustomAttributes','a:0:{}'),(12,2,'FilterStartDateDelta','-7'),(13,2,'FilterEndDateDelta','7'),(14,2,'FilterUserId','6'),(15,2,'FilterUserName',''),(16,2,'FilterScheduleId','0'),(17,2,'FilterResourceId','0'),(18,2,'FilterReservationStatusId','0'),(19,2,'FilterReferenceNumber',NULL),(20,2,'FilterResourceStatusId',NULL),(21,2,'FilterResourceReasonId',NULL),(22,2,'FilterCustomAttributes','a:0:{}'),(23,2,'FilterReferenceNumber',NULL),(24,2,'FilterResourceStatusId',NULL),(25,2,'FilterResourceReasonId',NULL);
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_resource_permissions`
--

DROP TABLE IF EXISTS `user_resource_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_resource_permissions` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `resource_id` smallint(5) unsigned NOT NULL,
  `permission_id` tinyint(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`,`resource_id`),
  KEY `user_id` (`user_id`),
  KEY `resource_id` (`resource_id`),
  CONSTRAINT `user_resource_permissions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_resource_permissions_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `resources` (`resource_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_resource_permissions`
--

LOCK TABLES `user_resource_permissions` WRITE;
/*!40000 ALTER TABLE `user_resource_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_resource_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_session`
--

DROP TABLE IF EXISTS `user_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_session` (
  `user_session_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `last_modified` datetime NOT NULL,
  `session_token` varchar(50) NOT NULL,
  `user_session_value` text NOT NULL,
  PRIMARY KEY (`user_session_id`),
  KEY `user_session_user_id` (`user_id`),
  KEY `user_session_session_token` (`session_token`),
  CONSTRAINT `user_session_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_session`
--

LOCK TABLES `user_session` WRITE;
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
INSERT INTO `user_session` VALUES (1,2,'2016-12-06 16:09:34','57f9149a16890','O:21:\"WebServiceUserSession\":19:{s:12:\"SessionToken\";s:13:\"57f9149a16890\";s:17:\"SessionExpiration\";s:24:\"2026-12-04T16:09:34+0000\";s:6:\"UserId\";s:1:\"2\";s:9:\"FirstName\";s:5:\"Admin\";s:8:\"LastName\";s:5:\"Admin\";s:5:\"Email\";s:20:\"studios@anglia.ac.uk\";s:8:\"Timezone\";s:13:\"Europe/London\";s:10:\"HomepageId\";s:1:\"1\";s:7:\"IsAdmin\";b:1;s:12:\"IsGroupAdmin\";b:0;s:15:\"IsResourceAdmin\";b:0;s:15:\"IsScheduleAdmin\";b:0;s:12:\"LanguageCode\";s:5:\"en_gb\";s:8:\"PublicId\";s:13:\"57f78370047d7\";s:9:\"LoginTime\";s:0:\"\";s:10:\"ScheduleId\";N;s:6:\"Groups\";a:1:{i:0;s:1:\"2\";}s:11:\"AdminGroups\";a:0:{}s:9:\"CSRFToken\";s:0:\"\";}');
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_statuses`
--

DROP TABLE IF EXISTS `user_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_statuses` (
  `status_id` tinyint(2) unsigned NOT NULL,
  `description` varchar(85) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_statuses`
--

LOCK TABLES `user_statuses` WRITE;
/*!40000 ALTER TABLE `user_statuses` DISABLE KEYS */;
INSERT INTO `user_statuses` VALUES (1,'Active'),(2,'Awaiting'),(3,'Inactive');
/*!40000 ALTER TABLE `user_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(85) DEFAULT NULL,
  `lname` varchar(85) DEFAULT NULL,
  `username` varchar(85) DEFAULT NULL,
  `email` varchar(85) NOT NULL,
  `password` varchar(85) NOT NULL,
  `salt` varchar(85) NOT NULL,
  `organization` varchar(85) DEFAULT NULL,
  `position` varchar(85) DEFAULT NULL,
  `phone` varchar(85) DEFAULT NULL,
  `timezone` varchar(85) NOT NULL,
  `language` varchar(10) NOT NULL,
  `homepageid` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `date_created` datetime NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastlogin` datetime DEFAULT NULL,
  `status_id` tinyint(2) unsigned NOT NULL,
  `legacyid` char(16) DEFAULT NULL,
  `legacypassword` varchar(32) DEFAULT NULL,
  `public_id` varchar(20) DEFAULT NULL,
  `allow_calendar_subscription` tinyint(1) NOT NULL DEFAULT '0',
  `default_schedule_id` smallint(5) unsigned DEFAULT NULL,
  `credit_count` decimal(7,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `public_id` (`public_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `user_statuses` (`status_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'System','Administrator','admin','studios@anglia.ac.uk','3c95c0a575608377b2e2ea4fea60a0ecd965c75d','019fbbaa','Tech Gods Anonymous','','','Europe/London','en_gb',1,'2016-10-06 16:08:11','2016-11-30 09:56:23','2016-11-30 09:56:23',1,NULL,NULL,'57f78370047d7',0,NULL,0.00),(3,'Nicholas','Townsend','npt1','nick.townsend@anglia.ac.uk','7c44a2853da2827a13e5a29153a3870fa11ac099','435aacf1','Cambridge','','','Europe/London','en_gb',1,'2016-10-07 13:41:43','2017-03-17 17:23:57','2017-03-17 17:23:57',1,NULL,NULL,'57f7a617e55e2',0,NULL,0.00),(5,'Nicholas Paul','Townsend','npt103','nicholas.townsend@student.anglia.ac.uk','d0559b21e6a7386ed9255263bb8531226e37c4dd','7a5f15ae',NULL,'Student',NULL,'Europe/London','en_gb',1,'2016-10-07 14:31:52','2016-11-30 09:55:29','2016-11-30 09:55:29',1,NULL,NULL,'57f7b1d9008d4',0,NULL,0.00),(7,'TEST','TECHNICIAN','test-tech','testtech@thejubster.co.uk','9ea37284aa7305a56599dfda982659208fe851e3','7210405b',NULL,NULL,NULL,'Europe/London','en_gb',1,'2016-10-11 23:45:07','2016-10-12 08:25:57','2016-10-12 08:25:57',1,NULL,NULL,'57fd7a9ee62bb',0,NULL,NULL),(9,'Mark','Pickering','mp26','mark.pickering@anglia.ac.uk','6c10ddafd3e5cacff1eed567938b55ab9e42f2cb','4056e7e4','','','','Europe/London','en_gb',1,'2016-10-18 13:16:36','2016-10-18 13:16:36',NULL,1,NULL,NULL,NULL,0,NULL,NULL),(10,'Mat','Skidmore','ms74','mat.skidmore@anglia.ac.uk','fb776f4f55f479d9d9a586f5403d6b2f1bd7e827','1a534204','','','','Europe/London','en_gb',1,'2016-10-18 13:16:36','2016-10-18 13:16:36',NULL,1,NULL,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-08 10:51:39
