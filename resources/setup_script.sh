#!/usr/bin/env bash
#
# Setup script for Booked VM


##10############################################################################
# CONFIG

# The user account to create for student use
DEV_USER='vmadmin'
# Password for above, leave blank for same as username
DEV_USER_PASSWORD=''

# The VM shared dir from which this script will run
# used for absolute paths
BASE_DIR='/vagrant'
RESOURCE_DIR='resources'

RESOURCE_PATH=$BASE_DIR"/"$RESOURCE_DIR

# Password for root and vagrant users
ROOT_PASSWORD='rootpw'
VAGRANT_PASSWORD='vagrantpw'

# Apache config files
APACHE_CONF_FILE='/etc/apache2/sites-available/000-default.conf'

# MySQL defaults

MYSQL_ROOT_PASSWORD='g00chy'

DB_IMPORT_FILE="$RESOURCE_PATH/import/booked-mysql-amt.sql"
DB_USER='booked_user'
DB_HOST='localhost'
DB_PASSWORD='g00b4b00b4'
DB_NAME='booked'
# app versions

BOOKED_VERSION='2.6.7'


# Additional apps to add to the VM
ADDITIONAL_PKGS=' apache2
                  git-core
                  curl
                  unzip
                  mysql-client
                  libpng12-dev
                  libjpeg-dev
                  libfreetype6-dev
                  libldap2-dev
                  php5-ldap
                  php5-mysql
                  mysql-server-5.5
                  libapache2-mod-php5
                  nodejs
                  npm

                '
                #                libjpeg62-turbo-dev


# Remove unnescary apps
REMOVE_PKGS='postgres* juju juju-core'

# Bail on error
set -e

# Print commands and output to ST DOUT
set -x

echo "Shell is $SHELL"

################################################################################
# UX CUSTOMISATIONS

# Configure timezone
locale-gen en_GB.UTF-8
timedatectl set-timezone Europe/London

# Change the password for vagrant and root user.
# This will ultimately stop fiddling and breaking
#echo ""root" "$ROOT_PASSWORD"" | chpasswd
#echo ""vagrant" "$VAGRANT_PASSWORD"" | chpasswd

################################################################################
# PKG INSTALLATION
if [ -n "$ADDITIONAL_PKGS" ] ; then
  # This will squash any interactive prompts (looking at you MariaDB/MySQL)
  export DEBIAN_FRONTEND=noninteractive
  # Install additional packages
  echo "Installing additional packages"
  apt-get update
  apt-get install -y $ADDITIONAL_PKGS
fi

if [ -n "$REMOVE_PKGS" ] ; then
  # Install additional packages
  echo "Removing packages"
  apt-get remove -y --purge $REMOVE_PKGS
fi

# Get rid of any other unused packages
apt-get autoremove -y


################################################################################
# Booked installation

# clone current version
cd /var/www

curl -L -Os https://sourceforge.net/projects/phpscheduleit/files/latest/booked-$BOOKED_VERSION.zip
unzip booked-$BOOKED_VERSION.zip

# Fix error in 2.6.7
# see:
# http://php.brickhost.com/forums/index.php?topic=15487.0
# http://php.brickhost.com/forums/index.php?topic=14472.0

sed -i 's@public function FilterCleared();@/*    public function FilterCleared();*/@g' /var/www/booked/Pages/SchedulePage.php

# copy over changes

cp $RESOURCE_PATH//app/call_reminders.php /var/www/booked/Web/call_reminders.php
#cp $RESOURCE_PATH/app/php.ini /usr/local/etc/php/php.ini
cp $RESOURCE_PATH/app/apache-header.conf /etc/apache2/conf-enabled/apache-header.conf
cp $RESOURCE_PATH/app/booked-config.php /var/www/booked/config/config.php
#cp $RESOURCE_PATH/app/booked-datadir/images /var/www/booked/Web/uploads/images
cp $RESOURCE_PATH/app/ActiveDirectory.config.php /var/www/booked/plugins/Authentication/ActiveDirectory/ActiveDirectory.config.php
cp $RESOURCE_PATH/app/log4php.config.xml /var/www/booked/config/log4php.config.xml
cp $RESOURCE_PATH/app/api/WebServiceExpiration.php /var/www/booked/Domain/Values/WebService/WebServiceExpiration.php
cp $RESOURCE_PATH/app/Web_Services.htaccess /var/www/booked/Web/Services/.htaccess
cp $RESOURCE_PATH/app/custom-logo.png /var/www/booked/Web/img/booked.png
cp $RESOURCE_PATH/app/lib/Email/Messages/ReservationEmailMessage.php /var/www/booked/lib/Email/Messages/ReservationEmailMessage.php
cp $RESOURCE_PATH/app/tpl/Email/emailheader.tpl /var/www/booked/tpl/Email/emailheader.tpl
cp $RESOURCE_PATH/app/tpl/Email/emailfooter.tpl /var/www/booked/tpl/Email/emailfooter.tpl
cp $RESOURCE_PATH/app/lang/en_us/ReservationCreated.tpl /var/www/booked/lang/en_us/ReservationCreated.tpl
cp $RESOURCE_PATH/app/lang/en_us/ReservationDeleted.tpl /var/www/booked/lang/en_us/ReservationDeleted.tpl
cp $RESOURCE_PATH/app/lang/en_gb.php /var/www/booked/lang/en_gb.php
cp $RESOURCE_PATH/app/tpl/Dashboard/admin_upcoming_reservations.tpl /var/www/booked/tpl/Dashboard/admin_upcoming_reservations.tpl
cp $RESOURCE_PATH/app/tpl/Dashboard/upcoming_reservations.tpl /var/www/booked/tpl/Dashboard/upcoming_reservations.tpl
cp $RESOURCE_PATH/app/Presenters/DashboardPresenter.php /var/www/booked/Presenters/DashboardPresenter.php

chown -R www-data:www-data /var/www/booked
chmod -R 0755 /var/www/booked

# Set dir for loggin

mkdir /var/log/booked

chown www-data:www-data /var/log/booked
chmod -R 0755 /var/log/booked

# configure apache to serve booked

sed -i 's,/var/www/html,/var/www/booked,g' /etc/apache2/sites-available/000-default.conf
sed -i 's,${APACHE_LOG_DIR},/var/log/apache2,g' /etc/apache2/sites-available/000-default.conf
#sed -i 's@\</VirtualHost>@g'
a2enmod rewrite
a2enmod headers

# Set up ssL

a2enmod ssl
cp $RESOURCE_PATH/ssl/*.pem /etc/ssl/certs/
cp $RESOURCE_PATH/ssl/*.key /etc/ssl/private/
chown www-data /etc/ssl/private/*
cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl-aru.conf
sed -i 's,/var/www/html,/var/www/booked,g' /etc/apache2/sites-available/default-ssl-aru.conf
sed -i 's/ssl-cert-snakeoil/cam-s-od01.anglia.local/g' /etc/apache2/sites-available/default-ssl-aru.conf
a2ensite default-ssl-aru

service apache2 restart

################################################################################
# MySQL setup

# Setup database for booked

# add user

mysql -u root -e "CREATE USER '$DB_USER'@'$DB_HOST' IDENTIFIED BY '$DB_PASSWORD'"
# create database
mysql -u root -e "CREATE DATABASE $DB_NAME"
# grant privileges
mysql -u root -e "GRANT SELECT, CREATE, UPDATE, INSERT, DELETE ON $DB_NAME.* TO $DB_USER@$DB_HOST"
mysql -u root -e "FLUSH PRIVILEGES"


if [ -f "$DB_IMPORT_FILE" ]; then

  echo "Importing existing database from $DB_IMPORT_FILE"

  mysql -u root $DB_NAME < $DB_IMPORT_FILE

  # Import existing DB

else

  echo "Creating default (dist) database"

  # create-schema.sql
  mysql -u root $DB_NAME < /var/www/booked/database_schema/create-schema.sql

  # create-data.sql
  mysql -u root $DB_NAME < /var/www/booked/database_schema/create-data.sql

  # full-install.sql
  sed -i "s/<booked_user>/$DB_USER/g" /var/www/booked/database_schema/full-install.sql
  sed -i "s/<booked>/$DB_NAME/g" /var/www/booked/database_schema/full-install.sql
  sed -i "s/<password>/$DB_PASSWORD/g" /var/www/booked/database_schema/full-install.sql
  mysql -u root $DB_NAME < /var/www/booked/database_schema/full-install.sql


fi

# We set MySQL up with no password for root.
# Whilst convenient for the above steps...
# It makes sense to set one, just in case
mysqladmin -u root password $MYSQL_ROOT_PASSWORD

### CRON
# Setup cron job for reminder emails

echo "Configuring cron"
crontab $RESOURCE_PATH/booked.crontab



################################################################################
# Freeboard setup

# Clone the freeboard repo into /var/www
cd /var/www/
git clone https://github.com/Freeboard/freeboard.git

# We use moment.js for date formatting so download this
curl -o /var/www/freeboard/js/moment.min.js https://momentjs.com/downloads/moment.min.js
# Include the moment.min.js script in the freeboard index.html and index-dev.html files
cd /var/www/freeboard
sed -i '/<script src="js\/freeboard.thirdparty.min.js"><\/script>/a    <script src="js\/moment.min.js"><\/script>' index.html
sed -i '/<script src="js\/freeboard.thirdparty.min.js"><\/script>/a    <script src="js\/moment.min.js"><\/script>' index-dev.html
npm install
# grunt

# Install the dashboard json file
mkdir /var/www/freeboard/boards
cp $RESOURCE_PATH/studios_dashboard.json /var/www/freeboard/boards

# Configure apache for Freeboard
awk '/<\/VirtualHost>/{gsub (/^/,"        Alias /dashboard /var/www/freeboard \n")}1' $APACHE_CONF_FILE  > /tmp/apacheconf
mv /tmp/apacheconf $APACHE_CONF_FILE
awk '/<\/VirtualHost>/{gsub (/^/,"        Alias /dashboard /var/www/freeboard \n")}1' /etc/apache2/sites-available/default-ssl-aru.conf  > /tmp/apacheconfssl
mv /tmp/apacheconfssl /etc/apache2/sites-available/default-ssl-aru.conf


# Reload apache to reflect changes
service apache2 restart

################################################################################
# Shutdown the machine
# shutdown -h now
