#!/bin/bash
#
# This script will attempt to backup all of the important shizzle from the booked VM
# The intention is to provide a snapshot of the important data that would be required to
# create a replica of the running system.
#
# This should not replace the weekly VM backup, but will run alongside.

# Configuration

SSH_PORT='2222'
SSH_HOST='localhost'
SSH_USER='vagrant'
LOCAL_BKP_PATH="./backups"
DB='booked'
#DATE=`date +%d%m%y`

# Dump db
# n.b. A script has been provided on the VM to do this.

ssh -p $SSH_PORT $SSH_USER@$SSH_HOST << 'END_PAYLOAD'
#  DATE=`date +%d%m%y`
  mysqldump -u root --password=g00chy booked > /home/vagrant/booked.sql
END_PAYLOAD

# Copy DB backup to host

if [ -f "$LOCAL_BKP_PATH/$DB-5.sql" ] ; then
    rm "$LOCAL_BKP_PATH/$DB-5.sql"
fi

if [ -f "$LOCAL_BKP_PATH/$DB-4.sql" ] ; then
  mv "$LOCAL_BKP_PATH/$DB-4.sql" "$LOCAL_BKP_PATH/$DB-5.sql"
fi

if [ -f "$LOCAL_BKP_PATH/$DB-3.sql" ] ; then
  mv "$LOCAL_BKP_PATH/$DB-3.sql" "$LOCAL_BKP_PATH/$DB-4.sql"
fi

if [ -f "$LOCAL_BKP_PATH/$DB-2.sql" ] ; then
  mv "$LOCAL_BKP_PATH/$DB-2.sql" "$LOCAL_BKP_PATH/$DB-3.sql"
fi

if [ -f "$LOCAL_BKP_PATH/$DB-1.sql" ] ; then
  mv "$LOCAL_BKP_PATH/$DB-1.sql" "$LOCAL_BKP_PATH/$DB-2.sql"
fi

if [[ -x ${LOCAL_BKP_PATH/$DB.sql} ]] ; then
  echo "Rotating $DB.sql to $DB-1.sql"
  mv "$LOCAL_BKP_PATH/$DB.sql" "$LOCAL_BKP_PATH/$DB-1.sql"
fi

scp -rp -P $SSH_PORT $SSH_USER@$SSH_HOST:/home/vagrant/$DB.sql "$LOCAL_BKP_PATH"

# Do something with the backed up data...
