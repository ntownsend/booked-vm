#!/bin/bash
# Retrieve members of specified AD groups and then obtain their Email address, FirstName and LastName
# Then write these to a CSV file in the format proscribed by booked(phpscheduleit)
#
# nick.townsend@anglia.ac.uk
# Oct 2016


# Config

# The groups for which to retrieve membership and user details

groups=("U0065FCAM01-Course-Students" "U0860SCAM01-Course-Students")
ID=`date +%s`


# GetGroupMembers
#
# Retrieves a list of members of the AD group passed to it
#

function GetGroupMembers () {

#    local members=$(dscl   /Active\ Directory/ANGLIA/anglia.local -read /Groups/U0065FCAM01-Course-Students  GroupMembership | sed  $'s@ANGLIA@@g' | sed 's@\\@@g')
    local members=$(dscl /Active\ Directory/ANGLIA/anglia.local -read /Groups/$1  GroupMembership | sed  $'s@ANGLIA@@g' | sed 's@\\@@g')
    echo $members
}

# GetMemberEmail
#
# Retrieves the stored EMailAddress entry for the specified user

function GetMemberEmail () {

    local user_email=$(dscl /Active\ Directory/ANGLIA/anglia.local -read /Users/$1 EMailAddress | cut -sd' ' -f2-)
    echo $user_email
}

# GetMemberFirstName
#
# Retrieves the stored FirstName entry for the specified user


function GetMemberFirstName () {
    local user_firstname=$(dscl /Active\ Directory/ANGLIA/anglia.local -read /Users/$1 FirstName | cut -sd' ' -f2-)
    echo $user_firstname

}

# GetMemberLastName
#
# Retrieves the stored LastName entry for the specified user


function GetMemberLastName () {
    local user_lastname=$(dscl /Active\ Directory/ANGLIA/anglia.local -read /Users/$1 LastName | cut -sd' ' -f2-)
    echo $user_lastname

}

# MakeCSV
#
# Creates a new CSV file ready to populate with data

function MakeCSV () {


    echo "username,email,first name,last name,password,phone,organization,position,timezone,language,groups"  > ./ad-booked-users-$ID.csv

}


# WriteToCSV
#
# Creates a csv entry for the specified user.
# This function calls GetMemberEmail, GetMemberFirstName and GetMemberLastName


function WriteToCSV () {
    # username, email, first name, last name,password, phone, organization, position, timezone,language, groups

    echo "$1,$(GetMemberEmail $1),$(GetMemberFirstName $1),$(GetMemberLastName $1),,,,,Europe/London,en_gb,Students" >> ./ad-booked-users-$ID.csv

}

### PAYLOAD

MakeCSV
# For each listed grop
for group in ${groups[*]}
do
    # Get the list of members
    echo "getting members of $group"
    members=$(GetGroupMembers $group)

    # For each member create a line for the CSV file
    for user in ${members[*]}
    do
        if [ $user != "GroupMembership:" ]; then
            echo "Searching AD for: $user"
            WriteToCSV $user && echo "added entry for user: $user"
            #printf "%s\n" $item
        fi
    done
done


