

#booked-db-backup.plist

##Install

curl http://launched.zerowidth.com/plists/ed729510-8433-0135-3a0e-1b7cccf8b92e/install | sh  | source 

Your .plist

`booked-db-backup.plist` running `/Users/sysadmin/Sandbox/vagrant/booked/db-backup.sh`

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Label</key>
	<string>com.zerowidth.launched.booked-db-backup</string>
	<key>ProgramArguments</key>
	<array>
		<string>sh</string>
		<string>-c</string>
		<string>/Users/sysadmin/Sandbox/vagrant/booked/db-backup.sh</string>
	</array>
	<key>StartCalendarInterval</key>
	<array>
		<dict>
			<key>Hour</key>
			<integer>7</integer>
			<key>Weekday</key>
			<integer>1</integer>
		</dict>
		<dict>
			<key>Hour</key>
			<integer>7</integer>
			<key>Weekday</key>
			<integer>2</integer>
		</dict>
		<dict>
			<key>Hour</key>
			<integer>7</integer>
			<key>Weekday</key>
			<integer>3</integer>
		</dict>
		<dict>
			<key>Hour</key>
			<integer>7</integer>
			<key>Weekday</key>
			<integer>4</integer>
		</dict>
		<dict>
			<key>Hour</key>
			<integer>7</integer>
			<key>Weekday</key>
			<integer>5</integer>
		</dict>
	</array>
	<key>UserName</key>
	<string>sysadmin</string>
	<key>WorkingDirectory</key>
	<string>/Users/sysadmin/Sandbox/vagrant/booked/</string>
</dict>
</plist>
```
Manual Install

Manual install:

`mkdir -p ~/Library/LaunchAgents`
`curl -o ~/Library/LaunchAgents/com.zerowidth.launched.booked-db-backup.plist http://launched.zerowidth.com/plists/ed729510-8433-0135-3a0e-1b7cccf8b92e.xml`
`launchctl load -w ~/Library/LaunchAgents/com.zerowidth.launched.booked-db-backup.plist`
Or, to install as root:

`curl -o ~/Downloads/com.zerowidth.launched.booked-db-backup.plist http://launched.zerowidth.com/plists/ed729510-8433-0135-3a0e-1b7cccf8b92e.xml`
`sudo cp ~/Downloads/com.zerowidth.launched.booked-db-backup.plist /Library/LaunchDaemons`
`sudo launchctl load -w /Library/LaunchDaemons/com.zerowidth.launched.booked-db-backup.plist`
Built by Nathan Witmer, hosted on heroku, source on github.